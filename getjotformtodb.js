const express = require("express")
// const path = require("path") 
const axios = require('axios')
const bodyParser = require('body-parser')
const mongo = require('mongodb').MongoClient

const app = express()
const port = "3011"

app.use(bodyParser.json())
app.listen(port, () => {
    console.log(`Listening to requests on http://localhost:${port}`)
}) 

app.get("/", (req, res) => {  
    res.send('Hai')  
})


const url = 'mongodb://localhost:27017'

const JOTFORM_CREDENT = {
    baseurl: 'https://api.jotform.com/',
    apiKey: 'e230c4bfee210a5af898e3f7da035b37',
    formID: '201671570436049'
}

mongo.connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}, (err, client) => {
    if (err) {
        console.error(err)
        return
    }
    const db = client.db('DataJotformLokal')
    const collectionSubmission = db.collection('datasubmission')


    //ENDPOINT untuk menarik data yg ada pada Server Jotform dan memasukannya ke DB Kita
    app.get("/tarikdatajotform", (req, res) => {  
        axios.get(`${JOTFORM_CREDENT.baseurl}form/${JOTFORM_CREDENT.formID}/submissions?apiKey=${JOTFORM_CREDENT.apiKey}`) //dokumentasi api jotform dpt di akses di :https://api.jotform.com/docs/#form-id-submissions
            .then(function (response) { 
                // console.log(JSON.stringify(response.data.content))
              
                // tujuan bulkwrite di bawah ini untuk melakukan proses insert bila data tidak ada, dan update bila data sudah ada sblmnya
                var ops = []
                response.data.content.forEach(item => {
                    ops.push(
                        {
                            updateOne: {
                                filter: { id: item.id },
                                update: {$set: {...item }}, //Agar Field 'approved' Tidak Tertimpa data yg datang, maka kita ubah jadi seperti ini
                                upsert: true
                            }
                        }
                    )
                })
                collectionSubmission.bulkWrite(
                    ops, 
                    { ordered: false },
                    function(error, result) {
                        if (error) {
                            console.log(err)
                            return res.send('gagal melakukan insert data jotform ke db cek log')
                        }
                        console.log('log insert', JSON.stringify(result))
                        res.send('Data DB Di Perbaharui !')  
                    }
                )

                 
            })
            .catch(function (error) {
                // handle error
                console.log(error)
                return res.send('gagal get data jotform cek log')
            }) 
    })


    //ENDPOINT untuk menampilkan data yg ada pada DB kita ke app client kita
    app.get("/getdata", (req, res) => {  
        const { approved } = req.query
        console.log(approved)
        const filter = approved ?      //cek apakah ada param approved ?, bila ada di baris berikutnya cek apa isi nilai param approved dan gunakan sebagai filter, bila tidak ada, maka get data dr db tanpa filter
            approved === 'true' ? 
                { approved: true } : 
                {$or: [ { approved: { $exists: false } } , { approved: false } ]} 
            : {}

        collectionSubmission.find(filter).toArray(function (err, items) {
            if (err) { 
                console.log(err)
                return res.send('gagal mendapatkan data dari db')
            }
            res.send(items)  
        })
    })

    app.post("/approve", (req, res) => {  
        const { id } = req.body
        console.log(id) 

        collectionSubmission.findOneAndUpdate(
            { id },
            {$set: { approved: true }},
            {
                returnOriginal: false
            },
            function (err, item) {
                if (err) { 
                    console.log(err)
                    return res.send('gagal meng-approve data')
                }
                // console.log(item)
                res.send(item.value)  
            }
        )
    })
})


